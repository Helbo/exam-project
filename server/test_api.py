import requests
import json

def test_api_get_crimes():
    # Define the URL of the API endpoint
    url = "http://bottle:4000/api/get-crimes"

    # Send a GET request to the endpoint
    response = requests.get(url)

    # Check the status code of the response
    if response.status_code == 200:
        print("Success Status code is 200.")
    else:
        print(f"Failure Expected status code 200, but got {response.status_code}.")



def test_api_relations():
    # Define the URL of the API endpoint
    url = "http://bottle:4000/api/relations/fakekey12345"

    # Send a GET request to the endpoint
    response = requests.get(url)

    # Check the status code of the response
    if response.status_code == 404:
        print("Success Status code is 404.")
    else:
        print(f"Failure Expected status code 404, but got {response.status_code}.")

def test_api_crimes_server():
    # Define the URL of the API endpoint
    url = "http://bottle:4000/api/crimes-server"

    # Send a GET request to the endpoint
    response = requests.get(url)

    # Check the status code of the response
    if response.status_code == 200:
        print("Success Status code is 200.")
    else:
        print(f"Failure Expected status code 200, but got {response.status_code}.")


def test_api_get_crimes_content():
    url = "http://bottle:4000/api/get-crimes"
    response = requests.get(url)
    
    if response.status_code == 200:
        try:
            # Directly parse the response body as JSON
            data = response.json()
            
            # Since the response does not wrap the crimes in a 'crimes' key,
            # we directly check the first element of the list
            first_crime = data[0]
            
            # Verify the presence of essential keys in the first crime object
            assert '_key' in first_crime, "Expected '_key' in the first crime object"
            assert 'severity' in first_crime, "Expected 'severity' in the first crime object"
            assert 'crime_type' in first_crime, "Expected 'crime_type' in the first crime object"
            assert 'time' in first_crime, "Expected 'time' in the first crime object"
            assert 'lon' in first_crime, "Expected 'lon' in the first crime object"
            assert 'lat' in first_crime, "Expected 'lat' in the first crime object"
            
            # Optionally, check for the presence of 'suspects', 'victims', and 'witnesses'
            assert 'suspects' in first_crime, "Expected 'suspects' in the first crime object"
            assert 'victims' in first_crime, "Expected 'victims' in the first crime object"
            assert 'witnesses' in first_crime, "Expected 'witnesses' in the first crime object"
            
            print("Content check passed.")
        except Exception as e:
            print(f"Content check failed: {e}")
    else:
        print(f"Failure Expected status code 200, but got {response.status_code}.")
